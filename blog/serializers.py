from rest_framework import serializers
from .models import TopHeadLines, Newsapi


class NewsapiSerializer(serializers.ModelSerializer):
    class Meta:
        model = Newsapi
        fields = ('__all__')


class TopheadSerializer(serializers.ModelSerializer):
    news_source = NewsapiSerializer()

    class Meta:
        model = TopHeadLines
        fields = ('__all__')
