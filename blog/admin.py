from django.contrib import admin

from .models import Newsapi, TopHeadLines

# Register your models here.
admin.site.register(Newsapi)
admin.site.register(TopHeadLines)
